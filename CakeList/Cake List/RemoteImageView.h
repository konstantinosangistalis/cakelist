//
//  RemoteImageView.h
//  Cake List
//
//  Created by Konstantinos Angistalis on 17/09/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemoteImageView : UIImageView

@property (nonatomic, strong) NSURL *imageURL;

@end
