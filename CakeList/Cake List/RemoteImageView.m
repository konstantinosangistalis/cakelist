//
//  RemoteImageView.m
//  Cake List
//
//  Created by Konstantinos Angistalis on 17/09/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import "RemoteImageView.h"


@interface RemoteImageView ()

@property (nonatomic, strong) NSURLSessionDataTask *imageTask;

@end

@implementation RemoteImageView

- (void)dealloc {
    // Stop the existing task, if any
    [self.imageTask cancel];
}

-(void)setImage:(UIImage *)image {
    
    if (self.imageTask != nil) {
        // Stop the existing task if the image is set normally
        [self.imageTask cancel];
        self.imageTask = nil;
    }

    _imageURL = nil;

    [super setImage:image];
}

- (void)setImageURL:(NSURL *)imageURL {
    
    if (self.imageTask != nil) {
        // Stop the existing task
        [self.imageTask cancel];
        self.imageTask = nil;
    }
    
    _imageURL = imageURL;

    if (imageURL != nil) {
        // Fetch the image data
        __weak typeof(self) blockSelf = self;
        
        self.imageTask = [[NSURLSession sharedSession] dataTaskWithURL:imageURL
                                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                          {
                              dispatch_sync(dispatch_get_main_queue(), ^{
                                  
                                  if (blockSelf && [blockSelf.imageURL.absoluteString isEqualToString:imageURL.absoluteString]) {
                                      // Only set the image if the cell was not reused
                                      blockSelf.imageTask = nil;
                                      UIImage *image = [UIImage imageWithData:data];
                                      [blockSelf setImage:image];
                                  }
                              });
                              
                          }];
        
        [self.imageTask resume];
    } else {
        [self setImage:nil];
    }
}

@end
