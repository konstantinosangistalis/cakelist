//
//  MasterViewController.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "MasterViewController.h"
#import "CakeCell.h"


@interface MasterViewController ()

@property (strong, nonatomic) NSArray *objects;
@property (strong, nonatomic) NSURLSessionDataTask *refreshTask;

@end


@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.refreshControl beginRefreshing];
    [self getData];
}

- (void)dealloc {
    [self.refreshTask cancel];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CakeCell *cell = (CakeCell*)[tableView dequeueReusableCellWithIdentifier:@"CakeCellID"];
    
    NSDictionary *object = self.objects[indexPath.row];
    cell.titleLabel.text = object[@"title"];
    cell.descriptionLabel.text = object[@"desc"];
 
    // Clear the previous image or set a placeholder while loading
    [cell.cakeImageView setImage:nil];
    
    NSURL *imageURL = [NSURL URLWithString:object[@"image"]];
    if (imageURL != nil) {
        // Set the url for the remote image
        cell.cakeImageView.imageURL = imageURL;
        
    } else {
        // Set a placeholder image for non-existing images
        [cell.cakeImageView setImage:nil];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)getData {
    
    if (self.refreshTask != nil) {
        // We already have a request in progress, skip this call
        return;
    }
    
    NSURL *url = [NSURL URLWithString:@"https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json"];
    
    __weak typeof(self) blockSelf = self;
    
    self.refreshTask = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                   completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                        {
                            if (blockSelf) {
                                
                                dispatch_sync(dispatch_get_main_queue(), ^{
                                    
                                    if (error != nil) {
                                        // Show failure alert
                                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                                                       message:[error localizedDescription]
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                                                                style:UIAlertActionStyleDefault
                                                                                              handler:^(UIAlertAction * action) {}];
                                        
                                        [alert addAction:defaultAction];
                                        [blockSelf presentViewController:alert animated:YES completion:nil];
                                        
                                    } else if (data != nil) {
                                        NSError *jsonError;
                                        id responseData = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:kNilOptions
                                                                                            error:&jsonError];
                                        if (!jsonError){
                                            blockSelf.objects = responseData;
                                            [blockSelf.tableView reloadData];
                                        } else {
                                            //
                                            NSLog(@"Parsing error: %@", [jsonError debugDescription]);
                                        }
                                    }
                                    
                                    [blockSelf.refreshControl endRefreshing];
                                    blockSelf.refreshTask = nil;
                                });
                            }
                        }];
    
    [self.refreshTask resume];
}


#pragma mark - Action Methods

- (IBAction)refreshAction:(UIRefreshControl *)sender {
    [self getData];
}

@end
